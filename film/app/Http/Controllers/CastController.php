<?php

namespace App\Http\Controllers;

use index;
use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function __construct ()
    {
        $this->middleware('auth');
    }

    public function index(){
        $cast = Cast::all();
        // dd ($cast);
        return view("pages.cast.cast", compact('cast'));
    }

    public function create(){
    	return view('pages.cast.create');
    }
 
    public function store(Request $request)
    {
    	// dd ($request->nama);
        $this->validate($request,[
    		'name' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);
 
        Cast::create([
    		'name' => $request->name,
    		'umur' => $request->umur,
            'bio' => $request->bio,
    	]);
 
    	return redirect()->route('cast');
    }

    public function edit($edit){
        $edit=Cast::where('id','=',$edit)->first();
        return view('pages.cast.edit', compact('edit'));
    }

    public function show($id){
        $show=Cast::where('id',"=",$id)->first();
        return view('pages.cast.show', compact('show'));
    }

    public function update($data, Request $request){
        $user=Cast::where('id',"=",$data)->update([ 
            'name' => $request->name,
            'umur' => $request->umur,
            'bio' => $request->bio,
         ]);
        return redirect()->route('cast');
    }

    public function delete($delete){
        $delete=Cast::where('id', '=',$delete)->delete();
        return redirect()->route('cast');
    }
}