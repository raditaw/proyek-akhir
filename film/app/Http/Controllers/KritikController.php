<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request){
        $this->validate($request,[
    		'point' => 'required',
    		'content' => 'required',
    	]);

        $komentar = new Kritik;

        $userid = Auth::id();

        $komentar->point = $request->point;
        $komentar->content = $request->content;
        $komentar->user_id = $userid;
        $komentar->film_id = $request->film_id;
        $komentar->save();

        return redirect('/film/'. $request->film_id);
    }
}
