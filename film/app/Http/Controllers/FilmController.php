<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use App\Kritik;
use Illuminate\Http\Request;
use Storage;

class FilmController extends Controller
{   
    public function __construct ()
    {
        $this->middleware('auth', ['except' => ['index','show']]);
    }

    public function create(){
        $daftarGenre = Genre::all();
        return view("pages.film.create", compact('daftarGenre'));
    }
    
    public function index(){
        $film = Film::all();
        return view("pages.film.film", compact('film'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|mimes:jpg,png,jpeg|max:2048',
        ]);

        $NamaPoster = time().'.'.$request->poster->extension();
        // dd($request->file('poster'));
        $poster = $request->file('poster')->store('uploads');

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $poster;
        $film->save();

        return redirect()->route('film');
    }

    public function edit($edit){
        $edit=Film::where('id','=',$edit)->first();
        $daftarGenre=Genre::all();
        return view('pages.film.edit', compact('daftarGenre', 'edit'));
    }

    public function show($id){
        $show=Film::where('id',"=",$id)->first();
        return view('pages.film.show', compact('show'));
    }

    public function update($id_film, Request $request){
        $data=Film::where('id', '=',$id_film)->first();

        if ($request->poster == null) {
            $gambarposter = $data->poster;   
        }else {
            Storage::delete($data->poster);
            $gambarposter = $request->file('poster')->store('uploads');
        }

        $film=Film::where('id', "=", $id_film)->update([ 
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'genre_id' => $request->genre_id,
            'poster' => $gambarposter
        ]);

        return redirect()->route('film');
    }

    public function delete($delete){
        $deletekritik=Kritik::where('film_id', '=', $delete)->delete();
        $deletedata=Film::where('id', '=', $delete)->first();
        Storage::delete($deletedata->poster);
        $delete=Film::where('id', '=', $delete)->delete();
        return redirect()->route('film');
    }
}
