<?php

namespace App\Http\Controllers;
use App\Film;
use App\Genre;
use App\Kritik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GenreController extends Controller
{
    public function __construct ()
    {
        $this->middleware('auth');
    }

    public function index(){
        $genre = Genre::all();
        // dd ($cast);
        return view("pages.genre.genre", compact('genre'));
    }

    public function create(){
    	return view('pages.genre.create');
    }
 
    public function store(Request $request)
    {
    	// dd ($request->nama);
        $this->validate($request,[
    		'name' => 'required',
    	]);
 
        Genre::create([
    		'name' => $request->name,
    	]);
 
    	return redirect()->route('genre');
    }

    public function show($id){
        $show=Genre::where('id',"=",$id)->first();
        return view('pages.genre.show', compact('show'));
    }

    public function delete($delete){
        $dataFilm=Film::where('genre_id', '=', $delete)->get();
        foreach ($dataFilm as $key => $value) {
            Storage::delete($value->poster);
            $deletekritik=Kritik::where('film_id', '=', $value->id)->delete();
        }

        Film::where('genre_id', '=', $delete)->delete();
        Genre::where('id', '=',$delete)->delete();
        return redirect()->route('genre');
    }
}
