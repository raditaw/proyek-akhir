@extends('layouts.main')

@section('main-content')

<div class="p-5">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Cast</h3>
        </div>
  
        <div class="card-body">
          <table id="table1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th style="width: 20% !important">ID</th>
              <th style="width: 20% !important">Name</th>
              <th style="width: 20% !important">Umur</th>
              <th style="width: 20% !important">Bio</th>
              <th style="width: 20% !important">action</th>
            </tr>
            </thead>
          <tbody>
            @foreach ($cast as $item)
              <tr> 
                <td>
                    {{ $item->id }}
                </td>
                <td>
                    {{ $item->name }}
                </td>
                <td>
                    {{ $item->umur }}
                </td>
                <td>
                    {{ $item->bio }}
                </td>
                <td>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route("show", $item->id) }}" class="btn btn-primary d-block"> show </a> 
                        <a href="{{ route("edit", $item->id) }}" class="btn btn-dark d-block mx-2"> Edit </a>
                        <form action="{{ route("delete", $item->id) }}" method="post" class="d-block form-delete">
                          @csrf @method('delete')    
                          <button type="submit" class="btn btn-danger delete"> delete </button>
                        </form>
                    </div>
                </td>
              </tr>
            @endforeach
          </tbody>
          </table>
          <a href="{{ route("create") }}" class="btn btn-primary">  Create Data </a>
        </div>
    </div>
  </div>
</div>

@endsection