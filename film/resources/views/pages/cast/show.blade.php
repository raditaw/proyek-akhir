@extends('layouts.main')
@push('styles')
@endpush

@section('main-content')

<div class="p-5">
  <div class="col-lg-12">
    <div class="card-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Umur</th>
            <th>Bio</th>
          </tr>
          </thead>
          <tbody>
            <td> {{ $show->id }} </td>
            <td> {{ $show->name }} </td>
            <td> {{ $show->umur }} </td>
            <td> {{ $show->bio }} </td>
          </tbody>
        </table>
    </div>
  </div>
</div>

@endsection