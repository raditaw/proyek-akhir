@extends('layouts.main')
@push('styles')

@section('main-content')

 <form action="{{ route("store") }}" method="POST">
     @csrf
    <div class="card">
        <div class="card-header">
            <h2> Tambah Data Cast </h2>
        </div>

        <div class="card-body">
            <div class="form-group">
                <label for="nama">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
               <label for="bio">BIO</label>
               <textarea name="bio" cols="30" rows="10" class="form-control" name="bio" id="bio" placeholder="Tuliskan Bio"> </textarea>
               @error('bio')
                   <div class="alert alert-danger">
                       {{ $message }}
                   </div>
               @enderror
           </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        </div>
    </div>

@endsection
