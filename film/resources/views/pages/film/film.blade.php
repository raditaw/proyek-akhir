@extends('layouts.main')

@push('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endpush

@section('main-content')
<div class="p-5">
    <a href="{{ route("create_film") }}" class="btn btn-primary btn-sm my-3"> Tambah Film </a>
    <div class="col-lg-12">
        <div class="card">
            <div class="row">
                @forelse ($film as $item)
                <div class="col-4">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset ('storage/'. $item->poster)}}" class="Responsive image">
                        <div class="card-body">
                            <h5> {{$item->judul}} </h5>
                            <h5> {{$item->genre->nama}} </h5>
                            <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
                            
                            @auth
                            <a href="{{ route("show_film", $item->id) }}" class="btn btn-primary">show</a>
                            <a href="{{ route("edit_film", $item->id) }}" class="btn btn-primary">edit</a>
                            <form action="{{ route("delete_film", $item->id) }}" method="post" class="d-block form-delete">
                            @csrf @method('delete')    
                            <button type="submit" class="btn btn-danger delete"> delete </button>
                            </form>
                            @endauth

                            @guest
                            <a href="{{ route("show_film", $item->id) }}" class="btn btn-primary">show</a>
                            @endguest
                        </div>
                    </div>
                </div>   

                @empty
                    <h3>Tidak Ada Daftar Film</h3>
                @endforelse    

            </div>
        </div>
    </div>
@endsection