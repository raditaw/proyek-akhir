@extends('layouts.main')

@push('style')
    
@endpush

@section('main-content')
<div class="p-5">
    <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
            <h3 class="card-title"> {{$show->judul}} </h3>
          </div>
          <div class="card-body">
              <img src="{{asset ('storage/'. $show->poster)}}" class="Responsive image">
                  <h5> {{$show->judul}} </h5>
                  <p> {{$show->tahun}} </p>
                  <p class="cals-text"> {{$show->ringkasan}} </p>
                  <hr>
                  <p> list Komentar </p>
                  @forelse ($show->komentar as $item)
                  <div class="card">
                      <h5 class="card-header"> {{ $item->user->name }} </h5>
                      <div class="card-body">
                          <h3 class="text-warning">Ratting {{ $item->point}}/5 </h3>
                          <h5 class="card-text"> {{$item->content}} </h5>
                      </div>
                  </div>
                  <br>
                  @empty
                      <p> Tidak ada komentar </p>  
                  @endforelse
                  <hr>
      
                  <h6> beri komentar </h6>
                  <form action="{{ route("kritik") }}" method="post" class="mt-4">
                      @csrf
                      <div class="form-group">
                          <select name="point" id="" class="form-control">
                              <option value=""> -- Rating Film -- </option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                          </select>
                      </div>
                      @error('point')
                      <div class="alert alert-danger"> {{ $message }}  </div>
                      @enderror
                      <br>
                      <div class="form-group">
                          <input type="hidden" value="{{ $show->id }}" name="film_id">
                          <textarea name="content" id="" cols="115" rows="10" placeholder="isi kritik/saran" class=""></textarea>
                      </div>
                      @error('point')
                      <div class="alert alert-danger"> {{ $message }}  </div>
                      @enderror
                      <input type="submit" class="btn btn-primary btn-sm" value="post">
                  </form>  
          </div>
      </div>
    </div>
</div>
@endsection