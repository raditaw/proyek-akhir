@extends('layouts.main')

@push('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endpush

@section('main-content')
 <form action="{{ route("store_film") }}" method="POST" enctype="multipart/form-data">
     @csrf
    <div class="card">
        <div class="card-header">
            <h2> Tambah Data Film </h2>
        </div>

        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" id="judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            {{-- @foreach ($daftarGenre as $item)
                @php
                    dd($item);
                @endphp
            @endforeach --}}

            <div class="form-group">
                <label for="exampleFormControlSelect1">Genre</label>
                <select class="form-control" id="exampleFormControlSelect1" name="genre_id">
                    <option value="">--PILIH SALAH SATU GENRE--</option>
                  @foreach ($daftarGenre as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endforeach
                </select>
              </div>

            <div class="form-group">
                <label for="judul">Ringkasan</label>
                <textarea name="ringkasan" cols="30" rows="10" class="form-control" name="ringkasan" id="ringkasan"> </textarea>
                @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="tahun">Tahun</label>
                <input type="text" class="form-control" name="tahun" id="tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="poster">Poster</label>
                <input type="file" class="form-control" name="poster" class="form-control" id="poster">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        </div>
    </div>

@endsection
