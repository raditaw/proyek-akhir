@extends('layouts.main')
@push('styles')

@section('main-content')

 <form action="{{ route("store_genre") }}" method="POST">
     @csrf
    <div class="card">
        <div class="card-header">
            <h2> Tambah Genre </h2>
        </div>

        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama Genre</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Genre">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        </div>
    </div>

@endsection
