@extends('layouts.main')
@push('styles')
@endpush

@section('main-content')
<div class="p-5">
  <h1> {{ $show->name }} </h1>
    <div class="col-lg-12">
        <div class="card">
          <div class="row">
            <div class="col-4">
              <div class="card" style="width: 18rem;">
                @forelse ($show->film as $item)  
                <img src="{{asset ('storage/'. $item->poster)}}" alt="Card image cap">
                <div class="card-body">
                  <h5>{{ $item->judul }}</h5>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
                  <a href="{{ route("show_film", $item->id) }}" class="btn btn-primary">show</a>
                </div>
                @empty
                <h1> tidak ada film </h1>
                @endforelse
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

{{-- <div class="row">

  <h1> {{ $show->id }} </h1>
  <p> {{ $show->name }} </p>
      @forelse ($show->film as $item)  
      <div class="p-5">
        <div class="col-lg-12">
            <div class="card">
                <div class="row">
                  <div class="col-4">
                      <div class="card">
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                          <h5>{{ $item->judul }}</h5>
                          <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
                          <a href="{{ route("show_film", $item->id) }}" class="btn btn-primary">show</a>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
      @empty
          <h1> tidak ada film </h1>
      @endforelse

</div> --}}

@endsection