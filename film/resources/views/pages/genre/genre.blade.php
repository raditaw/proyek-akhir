@extends('layouts.main')

@section('main-content')

<div class="p-5">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Genre</h3>
        </div>
  
        <div class="card-body">
          <table id="table1" class="table table-bordered table-striped">

            <thead>

            <tr>
              <th style="width: 20% !important">ID</th>
              <th style="width: 20% !important">Name</th>
            </tr>

            </thead>

          <tbody>
            @foreach ($genre as $item)
              <tr> 

                <td>
                    {{ $item->id }}
                </td>

                <td>
                    {{ $item->name }}
                </td>

                <td>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route("show_genre", $item->id) }}" class="btn btn-primary d-block"> show </a> 
                        <form action="{{ route("delete_genre", $item->id) }}" method="post" class="d-block form-delete">
                          @csrf @method('delete')    
                          <button type="submit" class="btn btn-danger delete"> delete </button>
                        </form>
                    </div>
                </td>
              </tr>
            @endforeach
          </tbody>
          </table>
          <a href="{{ route("create_genre") }}" class="btn btn-primary">  Tambah </a>
        </div>
    </div>
  </div>
</div>

@endsection