<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "IndexController@index")->name("Index");
Route::get("/dashboard", "IndexController@dashboard")->name("dashboard");

Route::get("/cast", "CastController@index")->name("cast");
Route::get("/cast/create", "CastController@create")->name("create");
Route::post("/cast", "CastController@store")->name("store");
Route::get('/cast/edit/{id}', 'CastController@edit')->name('edit');
Route::get('/cast/{cast_id}', 'CastController@show')->name('show');
Route::put('/cast/{cast_id}', 'CastController@update')->name('update');
Route::delete('/cast/{cast_id}', 'CastController@delete')->name('delete');

Route::get("/film", "FilmController@index")->name("film");
Route::get("/film/create", "FilmController@create")->name("create_film");
Route::post("/film", "FilmController@store")->name("store_film");
Route::get('/film/edit/{id}', 'FilmController@edit')->name("edit_film");
Route::put('/film/{film_id}', 'FilmController@update')->name('update_film');
Route::get('/film/{film_id}', 'FilmController@show')->name('show_film');
Route::delete('/film/{Film_id}', 'FilmController@delete')->name('delete_film');

Route::get("/genre", "GenreController@index")->name("genre");
Route::get("/genre/create", "GenreController@create")->name("create_genre");
Route::post("/genre", "GenreController@store")->name("store_genre");
Route::get('/genre/{genre_id}', 'GenreController@show')->name('show_genre');
Route::delete('/genre/{genre_id}', 'GenreController@delete')->name('delete_genre');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/kritik', 'KritikController@store')->name('kritik');

